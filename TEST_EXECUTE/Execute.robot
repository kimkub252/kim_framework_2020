*** Settings ***
Resource          ../KEYWORDS/AllKeywords.txt
Resource          ../FRAMEWORKS/AllResource.txt

*** Test Cases ***
TC_001_Kim
    @{Return}    [Excel] Get Data All Row For Excel    TestData    Data.xlsx    DATA    2
    [Google] Open Browser    @{Return}[1]    @{Return}[2]
    [Google] Input Text    @{Return}[3]
    [Google] Enter
    [W] Check Meaasge    @{Return}[3]    2s
    [Google] Get Message Google

TC_002_OpenExecel
    [Excel] Open Excel    TestData    Data.xlsx    Sheet1

TC_003_003_Get Data
    ${Return}    [Excel] Get Data All Row For Excel    TestData    Data.xlsx    DATA    2

TC_004_Testweb
    @{Return}    [Excel] Get Data All Row For Excel    TestData    Datatestweb.xlsx    DATA    2
    [W] Open Browser    https://getbootstrap.com/docs/4.0/examples/checkout/    Chrome
    [W] Input Text    id=firstName    @{Return}[1]
    [W] Input Text    id=lastName    @{Return}[2]
    [W] Input Text    id=username    @{Return}[3]
    [W] Input Text    id=email    @{Return}[4]
    [W] Input Text    id=address    @{Return}[5]
    [W] Input Text    id=address2    @{Return}[6]
    [W] Check Meaasge    United States
    [W] Select From List By Value    //*[@id="country"]    @{Return}[7]
    [W] Check Meaasge    California
    [W] Select From List By Value    id=state    @{Return}[8]
    [W] Input Text    id=zip    @{Return}[9]
    [W] Click Element Position    //*[@class="custom-control-label"]    1
    [W] Click Element Position    //*[@class="custom-control-label"]    2

TC_005_Login_Demoexex
    [Demoexex] Login
    [Demexex] Select Products
